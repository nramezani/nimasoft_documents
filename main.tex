%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4 paper

\IEEEoverridecommandlockouts                              % This command is only needed if 
                                                          % you want to use the \thanks command

\overrideIEEEmargins                                      % Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
\usepackage{graphics} % for pdf, bitmapped graphics files
\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
S-PR2 (Skilled-PR2): A python package for the kinematic control of PR2 robot*
}

\author{Nima Ramezani$^{1}$ and Mary-Anne Williams$^{2}$% <-this % stops a space
% \thanks{*This work was performed in the Magic-Lab in UTS \url{http://research.it.uts.edu.au/magic/Mary-Anne/}}% <-this % stops a space
\thanks{$^{1}$Nima Ramezani is with Faculty of Engineering, and Information Technology,
        University of Technology Sydney, Broadway, Ultimu, NSW 2007, Australia
        {\tt\small Nima.RamezaniTaghiabadi@uts.edu.au}}%
\thanks{$^{2}$Mary-Anne Williams is with Faculty of Engineering, and Information Technology,
        University of Technology Sydney, Broadway, Ultimu, NSW 2007, Australia
        {\tt\small Mary-Anne@themagiclab.org}}%
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

S-PR2 is a Python based software package which can be used as a kinematic control toolbox for PR2 service robot. The software exploits an analytic Inverse Kinematic solver for PR2 redundant arm (7 DOF) with capability of optimum redundancy resolution. S-PR2 can be used by robot researchers and programmers to generate their desired trajectories in the operational space and execute on a physical robot or in simulation. The package gives PR2 robot, the skills of tracking a user-defined trajectory like writing letters or drawing shapes on the board.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

An efficient and reliable software package for optimal and feasible kinematic control of redundant robots is a vital requirement 
for researchers who work on robot manipulation. Despite of considerable improvements in the Inverse Kinematic (IK) solution methods proposed in recent years, these techniques have not been adopted by most of the existing IK solver software solution. The existing IK solver for PR2, which is a part of Robot Operating System (ROS), is simple and fast but lacks some important advanced features required for generating smooth and reliable trajectories. It only projects a single pose to the joint-space and not a trajectory, and even for one single pose it does not give all available solutions (corresponding to a certain value of a redundant parameter). The IK solver does not exploit available redundancy in Degree of Freedom (DOF)  to minimise desired cost function or fulfil additional constraints. For example, for 7 DOF PR2 Arm, an advanced IK solver should be able to  provide a solution subject to any arbitrary value for the Shoulder-Pan joint. The current solver cannot do this. It also cannot give an \emph{Internal Motion Trajectory} connecting two points in the solution manifold. Furthermore, the IK solver does not return a feasibility set for a redundant parameter to be chosen. Operationally, the PR2 IK solver is fully embedded within the ROS system and cannot be easily replaced and migrated to other systems.

%This is mainly due to the fact that developing a software for realtime robot control requires extensive knowledge and expertise in programming and deep understanding of the robot platform. Kinematicians who design advanced IK algorithms usually do not have such skills.

This paper introduces a comprehensive kinematic control software package for PR2 robot, S-PR2, that is based on an analytic inverse kinematic solution[ref] that resolves aforementioned issues. S-PR2 generates more effective robot motion plans and hence it can be used to produce more reliable robot behaviours. In S-PR2, the method used for calculating redundancy resolution exploits the advantage of existence of an analytic IK solution for improving an iterative redundancy optimization in terms of reliability, accuracy, and computational cost where closed-form IK equations are used to reduce dimensionality and complexity of the objective function. The redundancy optimization is implemented in two control modes with seven and eleven degrees of freedom respectively:
\begin{itemize}
 \item \textbf{Fixed-mode} in which the trunk is fixed and only the seven arm joints can move.
 \item \textbf{Free-mode} in which the trunk is free to navigate and lift, while the arm joints can also change.
 In this mode, the system will have eleven degrees of freedom.(11 DOF)
\end{itemize}

S-PR2 allows an end user to easily generate desired trajectories in tasks-pace, project them to the joint-space while exploiting DOF redundancy, visualize the trajectories, and finally, execute them on a PR2 robot using simple Python function calls. The typical user does not need any specific knowledge of the underlying robot control system in order to use S-PR2. Expert robot programmer can, however, use the lower level function modules in S-PR2 to write customized IK control system. Since, S-PR2 is written in pure Python scripting language, the kinematic software is agnostic to the underlying operation platform. In the following sections, 
% simultaneous arm motion, or running different trajectories for two arms simultaneously.
we will first describe the system structure and core functional modules in S-PR2, then each subcomponent will be briefly explained in terms of their methods and capabilities. Finally, an example of a practical application of the software package is demonstrated.

\section{System Architecture}
\label{sec_system_structure}
S-PR2 is implemented in a hierarchical structure in which higher level classes inherit and extends functionalities of the lower level classes.
The overall structure of S-PR2 is shown in Figure \ref{fig_structure}. The core module is \textbf{PR2-Arm-Kinematics} (Section \ref{sec_pr2_arm_kin_mod}) that contains all required kinematic methods, including an analytic arm IK solver and an optimal trajectory planner. 
This component inherits joints-pace methods and properties from a generalized class called \textbf{Manipulator-Configuration}. Each module instance is constructed with an ensemble of the settings specific to the object. For example, \textbf{Manipulator-Configuration-Settings} contains general joint-space settings of a manipulator such as mechanical limits, types of the joints and a set of weightings for each joint that 
is used in defining a user-defined objective function. The settings in the \textbf{PR2-Arm-Kinematic} module contains the robot main dimensions and algorithmic settings that are used for the inverse kinematics and redundancy optimization calculation. The entire kinematics of the robot are computed in a higher level module called \textbf{PR2-Kinematics}. This module supports a comprehensive kinematic engine for PR2 using the IK solvers of the arms. \textbf{PR2-Kinematics} contains two instances of \textbf{PR2-Arm-Kinematic} for both the right and the left arms.
A user can define a target pose or a pose trajectory for each of the arm end effector in a global reference coordinate system and solve the IK or project the trajectory into the joint-space. The free-base mode optimizer computes the optimum trunk position, rotation angle, and trunk height for a desired End Effector (EE) pose. All above modules perform necessary computations and are completed independent of the actual robot operations. Access to the physical robot platform is established by a connector module \textbf{PR2-Synchronizer} that carry out control command and data mapping and communicate them to a middleware \textbf{PyRIDE} \cite{} which acts as an intermediary agent to the low level robot control system.

\begin{figure}
  \centering 
  \framebox{\parbox{3in}{
  \includegraphics[width=1.0\linewidth]{figures/spr2_structure_diagram2}
  }}
  \caption{Architecture diagram of S-PR2}
  \label{fig_structure}
\end{figure}

The actual communication with PyRIDE is done using \textbf{pyride-interpreter} interface module. Finally, \textbf{Skilled-PR2} inherited from \textbf{PR2-Synchronizer} is the final module in the package with the highest level of functionalities. This module supports basic primitive motion skills like moving in various directions, running an internal motion into a low cost configuration or trajectory tracking. The end user can create his/her own PR2 controller class inherited from \textbf{Skilled-PR2} and add more skills. \textbf{Writer-PR2} (Section \ref{sec_application}) is a working example designed to provide the ability of writing letters for PR2.

\section{PR2 Arm Kinematic Module}
\label{sec_pr2_arm_kin_mod}

The arm kinematic module is the main kinematic engine of S-PR2 with the lowest level functionalities.
This module provides all the required methods for kinematic computations of the arm.

\subsection{IK Solver in Fixed-Base Mode}
The most important part of the module is an analytic IK solver that can give all the feasible configurations corresponding to a desired EE pose
for an arbitrary value of the redundant parameter (up to maximum 8 solutions). The redundant parameter is by default selected as the first arm joint named \emph{Shoulder-Pan Joint}. If the given task pose is outside the arm reachable region or an ill chosen value is used for the redundant parameter, no solution can be found by the solver. Since identifying the reason for failing to find a solution can be difficult, S-PR2  provides a feasibility \emph{Permission Set} (Subsection \ref{ssec_permission_set}) for the redundant parameter and searches
within this set to find a value that leads to solution with respect to the user's desired criterion, e.g. the distance of the current configuration from the middle of the joint ranges. If the computed permission set is empty, then it is certain that the requested task pose lies outside of the robot workspace so that no valid solution exists.

\subsection{Permission Set} 
\label{ssec_permission_set}
In IK problems, the key challenge is to use redundancy to find the optimal joint values within their feasible ranges. Each joint limitation excludes part of the solution manifold and imposes a restriction on the set from which the redundant parameters can be chosen. For PR2 that has only one redundant parameter in its manipulator, a \emph{Permission Set} consisting of a union of intervals can be obtained by the intersection of feasibility permission sets imposed on the redundant parameter by the limitations of each joint. In other words, a permission set is a set from which the redundant parameter must be selected otherwise no solution in the feasible range can be found for the desired EE pose.
S-PR2 computes the permission set for a given desired EE pose using \emph{Arithmetic of Intervals} \cite{GOZE2009}.

\subsection{Optimal Redundancy Resolution}
% Definitions:
Optimal use of redundancy in motion planning has been the subject to extensive research in Robotics[ref]. A task in the cartesian space requires six kinematic constraints for the EE. If the DOF or the number of free joints is greater than the number of constraints required to fulfil a task, the robot is known as \emph{Redundant}. In this case, one or some of the joints can be arbitrarily chosen as \emph{redundant parameters}. It is also possible to consider a user defined function of joints as a redundant parameter. Selecting an appropriate function for redundant parameters is called \emph{parametrization} and it depends upon the geometry of the manipulator. Each redundant parameter adds an additional constraint to the system, so the total number of redundant parameters named as \emph{Degree of Redundancy} is computed as $ r = n - m$ where $n$ is the degree of freedom and $m$ is the number of main kinematics constraints ($m = 6$ for a desired EE position and orientation). Some redundancy parametrizations have been proposed for specific geometries, like Lee and Bejczy \cite{1991_conf_Lee.Bejczy_Redundant} who used some of the joints as redundant parameters in a 8 DOF arm or Shimizu et. al. \cite{2008_arti_Shimizu.Kakuya.ea_Analyticala}, who used a user defined angle as the redundant parameter in a 7 DOF arm.
The analytic solution of IK in redundant systems is to express joint positions as closed-form functions of redundant parameters, and 
the problem of \emph{Optimal Redundancy Resolution} in analytic IK is to find a feasible value for the redundant parameter(s) to 
minimize a desired cost function subject to some additional constraints.

% Methodology:
The method we used for redundancy resolution in fixed-base mode is similar to Shimizu's work \cite{2008_arti_Shimizu.Kakuya.ea_Analyticala} but with a different parametrization. The analytic IK method used is also different as their formulations could not be used for the PR2 arm due to a slight difference in the Denavit-Hartenberg (DH) parameters \cite{1955_arti_Denavit.Hartenberg_kinematic}.

\subsection{Smooth Trajectory Projection}
The arm kinematic module can project a trajectory from task-space into the joint-space. Trajectories are instances of module \textbf{Trajectory} consisting of segments which are established from a number of key points containing positions, velocities and accelerations. 
Each key point can apply constraint(s) to the trajectory. Trajectory points are interpolated from a fitted spline in form of a polynomial or Fourier series. The user can visualize both joint-space and task-space trajectories before executing them on a physical robot. \textbf{Orientation-Trajectory} is a module inherited from \textbf{Trajectory} supported by various methods for smooth orientation trajectory interpolation \cite{1997_arti_Park.Ravani_Smooth}.Given the key orientations and corresponding angular velocities and accelerations
the interpolated orientation can be computed at any phase value using methods like SLERP \cite{Kremer2008} with various conventions like quaternions \cite{2008_arti_Kavan.Collins.ea_Geometric}. The module also supports polynomial or Fourier interpolation using one of the vectorial non-redundant representations of three-dimensional rotation \cite{2003_arti_Bauchau.Trainelli_Vectorial}.

In trajectory generation, by default, the redundancy is used to minimize the deviation from current joint values. This ensures a smooth trajectory which is hard to generate without an optimization.The user can define his/her own objective function in terms of joint angles by changing the settings of each arm module. For example, one can select a weighted cost function reflecting deviation from a certain point in the joint-space.

\subsection{Internal Motion Generation}
In a redundant manipulator, if the end effector is fixed in some pose,  the joints can still change in the solution manifold to generate a motion that does not influence the EE pose. This is known as the \emph{Internal Motion}. Different values of the joints in the solution manifold can be achieved by changing the redundant parameters. The arm kinematic module in S-PR2 is able to generate a smooth internal motion within the solution manifold, connecting two different IK solution points in the joint-space. This enables the robot to change itself into a better configuration while the EE pose is fixed.

% 
% $$
% \alpha + \beta = \chi \eqno{(1)}
% $$

% Note that the equation is centered using a center tab stop. Be sure that the symbols in your equation have been defined before or immediately following the equation. Use �(1)�, not �Eq. (1)� or �equation (1)�, except at the beginning of a sentence: �Equation (1) is . . .�

\section{PR2 Kinematics Module}
PR2 is a wheel-based service robot with two 7-DOF arms, a tilting head and a sliding joint, adjusting the working height of the arms and adding one degree of freedom to the system. The navigating platform can move on the floor and rotate around $z$ axis providing three
extra degrees of freedom. In free-base mode, eleven degrees of freedom kinematically influence the position or orientation of one gripper.
The PR2 kinematic module is a comprehensive toolbox for all kinematic computations of PR2. It exploits the arm local IK solver to compute the configuration in terms of eleven joints (degrees of freedom) given the desired EE pose in the global coordinate system. This module also generates trajectories for each of the eleven degrees of freedom for a desired arm task.

\subsection{Redundancy Resolution in Free-Base Mode}

In free-base mode, the redundancy resolution technique used for the arm is extended to five redundant parameters for the entire robot.
In free-base mode, the position, orientation and height of the robot trunk are computed so that a desired cost function is minimized.
This is another useful feature that can not be found in any of the existing packages for PR2. With the closed form arm IK equations, the redundancy optimization problem is transferred from the eleven dimensional joint-space into a five dimensional solution manifold spanned by the redundant parameters. In this method, gradient of the objective function with respect to redundant parameters are computed 
to find an optimum direction for the change of redundant parameters and hence the joint values.

% 
% \subsection{Figures and Tables}
% 
% Positioning Figures and Tables: Place figures and tables at the top and bottom of columns. Avoid placing them in the middle of columns. Large figures and tables may span across both columns. Figure captions should be below the figures; table heads should appear above the tables. Insert figures and tables after they are cited in the text. Use the abbreviation �Fig. 1�, even at the beginning of a sentence.
% 
% \begin{table}[h]
% \caption{An Example of a Table}
% \label{table_example}
% \begin{center}
% \begin{tabular}{|c||c|}
% \hline
% One & Two\\
% \hline
% Three & Four\\
% \hline
% \end{tabular}
% \end{center}
% \end{table}
% 

\section{Application}
\label{sec_application}

\begin{figure}
  \centering 
  \framebox{\parbox{3in}{
  \includegraphics[width=1.0\linewidth]{figures/S_3D_trajectory}
  }}
  \caption{A user-defined 3D trajectory for writing letter S on a flat surface. 
  The EE goes to the initial point of the letter, then moves forward and writes the letter,
  and then moves backward and goes to the position to write the second letter.
  The units are meters for all the axis.}
  \label{fig_3d_traj}
\end{figure}


Writing and drawing are complicated tasks that require high accuracy and well cognition of the environment. We used S-PR2 to create a \textbf{Writer-PR2} that enables PR2 to write letters or draw any desired shape on the board.

\begin{figure}
  \centering 
  \framebox{\parbox{3in}{
  \includegraphics[width=1.0\linewidth]{figures/ts_traj}
  }}
  \caption{Axis Curves for the user-created taskspace trajectory shown in Figure \ref{fig_3d_traj}. 
  See the the positions for each coordinate in the left side and equivalent velocities in the right.
  Trajectory keypoints are shown by small green circles.
  The units are meters for distance and meters/second for velocities.}
  \label{fig_ts_traj}
\end{figure}

Raw trajectories are given by an external device such as an iPad tablet or loaded from a file containing some predefined trajectories
(for example for English alphabets), then mapped into the desired size, location, and orientation. They are then projected into a feasible joint-space trajectory according to the current robot state and configuration. The shapes or letters are drawn/written by any of the arms with desired size and speed. Figure \ref{fig_3d_traj} shows the task-space raw trajectory for letter \textbf{S}. The corresponding task-space and projected joint-space trajectories are illustrated in Figures \ref{fig_ts_traj} and \ref{fig_ts_traj}.

% Figure \ref{} shows PR2 after it has successfully drawn the given shape.

\begin{figure} 
  \centering 
  \framebox{\parbox{3in}{
  \includegraphics[width=1.0\linewidth]{figures/js_traj}
  }}
  \caption{Left arm jointspace trajectories for writing letter S with height $10 cm$,
  when the gripper orientation is perpendicular to the board held by the right arm}
  \label{fig_ts_traj}
\end{figure}



\addtolength{\textheight}{-12cm}   % This command serves to balance the column lengths
                                  % on the last page of the document manually. It shortens
                                  % the textheight of the last page by a suitable amount.
                                  % This command does not take effect until the next page
                                  % so it should come on the page before the last. Make
                                  % sure that you do not shorten the textheight too much.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section*{APPENDIX}
% 
% Appendixes should appear before the acknowledgment.
% 
% \section*{ACKNOWLEDGMENT}
% 
% The preferred spelling of the word �acknowledgment� in America is without an �e� after the �g�. Avoid the stilted expression, �One of us (R. B. G.) thanks . . .�  Instead, try �R. B. G. thanks�. Put sponsor acknowledgments in the unnumbered footnote on the first page.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{plain}

%\begin{thebibliography}{99}

% \bibitem{c1} S. Lee and A.K. Bejczy, Redundant Arm Kinematic Control Based on Parametrization, Proceedings of the IEEE International Conference on Robotics and Autmation, 
% \bibitem{c2} W.-K. Chen, Linear Networks and Systems (Book style).	Belmont, CA: Wadsworth, 1993, pp. 123�135.
% \bibitem{c19} N. Kawasaki, �Parametric study of thermal and chemical nonequilibrium nozzle flow,� M.S. thesis, Dept. Electron. Eng., Osaka Univ., Osaka, Japan, 1993.
% \bibitem{c20} J. P. Wilkinson, �Nonlinear resonant circuit devices (Patent style),� U.S. Patent 3 624 12, July 16, 1990. 


\bibliography{../../publications/robotics/robotics_bibliography,../../publications/mathematics/mathematics_bibliography,../../publications/computer_graphics/computer_graphics_bibliography}

% \end{thebibliography}

\end{document}
